const express = require('express');
const app = express();
const cors = require('cors');
const morgan = require('morgan');
const request = require('request');
const bodyParser = require('body-parser');
const firebase = require('firebase');

// Enable all CORS requests
app.use(cors());
/* set up the third party API */
const url = 'http://api.openweathermap.org/data/2.5/weather?';

// Initialize Firebase & store config info to a const
var config = {
  apiKey: "AIzaSyDPWurayZstHz5L5IxV_58MX12vyKt39lg",
  authDomain: "restfulapi-1433.firebaseapp.com",
  databaseURL: "https://restfulapi-1433.firebaseio.com",
  projectId: "restfulapi-1433",
  storageBucket: "restfulapi-1433.appspot.com",
  messagingSenderId: "81213069936"
};
var firebase_app = firebase.initializeApp(config);

// Get a weather database reference
const weather_db = firebase_app.database().ref("weathers");

// Get a user database reference
const user_db = firebase_app.database().ref("users");

// Get a bookmark database reference
const bookmark_db = firebase_app.database().ref("bookmarks");

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));


// *****************************
// Home page
// *****************************
// Show the information of main page
app.get('/', function(req, res) {
  res.status(200).json({
    user: './users',
    weather: './weathers',
    bookmark: './bookmarks'
  });
});

// *****************************
// Weathers api
// *****************************
// Get all weathers from firebase 
app.get('/weathers', function(req, res) {
  getAllWeathers(function(arr) {
    res.status(200).json(arr);
  });
});

// Add a weather to firebase
app.post('/weathers', (req, res) => {
  var cityName = req.body.cityName + "";
  firebase_app.database().ref('weathers/' + cityName).set(req.body);
  res.status(201).json({
    message: 'A new weather added successfully',
    createdWeather: req.body
  });
});

// Get a weather info from openweather
app.get('/weathers/:City', (req, res, next) => {
  // Get from openweather
  const city = req.params.City.toString().trim();
  const units = req.query.units;
  getWeatherByCity(city, units, function(arr) {
    res.status(200).json(arr);
  });
});

// Update the firebase weather info with openweather info
app.patch('/weathers/:City', (req, res) => {
  // Get from openweather
  const city = req.params.City.toString().trim();
  getWeatherByCity(city, function(arr) {
    firebase_app.database().ref('weathers/' + city).set(arr);
    res.status(200).json({
      message: 'Weathers updated successfully',
      updatedWeather: arr
    });
  });
});

// Delete a weather from firebse
app.delete('/weathers/:City', (req, res, next) => {
  const cityName = req.params.City;
  firebase_app.database().ref('weathers/' + cityName).set({});
  res.status(200).json({
    message: 'Weather ' + cityName + ' deleted successfully'
  });
});

// *****************************
// Bookmarks api
// *****************************
// Get all bookmarks info
app.get('/bookmarks', function(req, res, next) {
  getAllBookmarks(function(arr) {
    res.status(200).json(arr);
  });
});

// Add a new bookmark
app.post('/bookmarks', function(req, res, next) {
  var userid = req.body.userid + "";
  firebase_app.database().ref('bookmarks/' + userid).set(req.body);
  res.status(201).json({
    message: 'A new bookmark added successfully',
    createdBookmark: req.body
  });
});

// Get a bookmark info 
app.get('/bookmarks/:userid', (req, res, next) => {
  const userid = req.params.userid;
  getBookmarksByID(userid, function(arr) {
    res.status(200).json(arr);
  });
});

// Add a bookmark to user
app.put('/bookmarks/:userid', (req, res, next) => {
  const userid = req.params.userid;
  var citiesDB = firebase_app.database().ref('bookmarks/' + userid + '/cities/');
  citiesDB.once('value').then(function(snap) {
    var num = snap.val().length;
    citiesDB.child(num).set(req.body);
    res.status(201).json({
        message: 'Bookmark added successfully',
        bookmarkId: req.params.userid
    });
  });
});

// Delete bookmarks of a user
app.delete('/bookmarks/:userid', (req, res, next) => {
  const userid = req.params.userid;
  firebase_app.database().ref('bookmarks/' + userid).set({});
  res.status(200).json({
    message: 'Bookmark deleted successfully',
    bookmarkId: req.params.userid
  });
});

// *****************************
// Users api
// *****************************
// Get all users info from firebase
app.get('/users', function(req, res) {
  getAllUsers(function(arr) {
    res.status(200).json(arr);
  });
});

// Add a new user to firebase
app.post('/users', function(req, res) {
  var userid = req.body.userid + "";
  firebase_app.database().ref('users/' + userid).set(req.body);
  res.status(201).json({
    message: 'A new user added successfully',
    createdUser: req.body
  });
});

// Get a user info
app.get('/users/:userid', function(req, res) {
  const userid = req.params.userid;
  getUserByID(userid, function(arr) {
    res.status(200).json(arr);
  });
});

// Delete a user
app.delete('/users/:userid', (req, res, next) => {
  const userid = req.params.userid;
  firebase_app.database().ref('users/' + userid).set({});
  res.status(200).json({
    message: 'Delete user ' + userid + ' successfully'
  });
});
// *****************************

// Handle the Cross-Origin Resource Sharing
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Authorization, Accept, X-Requested-With");
  res.header("Content-Type", "application/json;charset=utf-8");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET, OPTIONS');
  next();
});

// 404 error handling
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

// 500 error handling
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

function getAllWeathers(callback) {
  weather_db.once('value').then(function(snap) {
    callback(snap.val());
  });
}

function getAllUsers(callback) {
  user_db.once('value').then(function(snap) {
    callback(snap.val());
  });
}

function getAllBookmarks(callback) {
  bookmark_db.once('value').then(function(snap) {
    callback(snap.val());
  });
}

function getUserByID(id, callback) {
  user_db.child(id).once('value').then(function(snap) {
    callback(snap.val());
  });
}

function getBookmarksByID(id, callback) {
  bookmark_db.child(id).once('value').then(function(snap) {
    callback(snap.val());
  });
}

function getWeatherByCity(cityName, units, callback) {
  const query_string = { q: cityName, units: units, appid: "2627467fdec2eff8c05fe1a0215b69be" };
  request.get({ url: url, qs: query_string }, function(err, res, body) {
    if (err) {
      console.log(err);
    }
    else {
      const json = JSON.parse(body);
      var return_array = {};
      if (return_array[json.name] == undefined) {
        return_array[json.name] = [];
      }
      return_array = {
        temp: json.main.temp,
        min: json.main.temp_min,
        max: json.main.temp_max,
        desc: json.weather[0].description,
        iconUrl: json.weather[0].icon,
        cityName: json.name
      };
      callback(return_array);
    }
  });
}

// commonJS Exports standard
module.exports = app;
