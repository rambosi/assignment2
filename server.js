const http = require('http');
// Require 'app' module CommonJS standard
const app = require('./app')
const port = process.env.PORT || 3000
var server = http.createServer(app);

server.listen(port);
